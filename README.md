# Kubernetes Cluster on Raspberry Pis
Configuration for a Raspberry Pi Kubernetes Cluster.

## Dependencies
All you need on your managing machine (not to be confused with the master node) is the SSH client, setup properly for the command line, and [Ansible](https://www.ansible.com/), which is used to configure the machines via SSH.

## Setup
1. Setup hardware and network
1. Flash SD cards with [HypriotOS](https://github.com/hypriot/image-builder-rpi/) and boot Pis
1. Configure SSH key-based access on all Raspberry Pis
1. Replace IP adresses in `raspberry-pis.ini` file
1. (optional) Check if Ansible is configured correctly by running `ansible nodes -i ./raspberry-pis.ini -m ping`

## Thanks
Big Thanks to the people at [Bee42](https://bee42.com/) for their introductory Kubernetes Day and helping me fixing some nasty bugs.
Also to Kasper Nissen for his blog article on Kubernetes on Raspberry Pi and the team of HypriotOS
